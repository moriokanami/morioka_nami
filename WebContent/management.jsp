<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/management.css" rel="stylesheet" type="text/css">
		<title>ユーザー管理画面</title>
		<script type="text/javascript">
		function check(){
			if(window.confirm('実行してよろしいですか？')){
				return true;
			}
			else{
				window.alert('キャンセルされました');
				return false;
			}
		}
		</script>
	</head>
	<body>
		<div id="header">
			<a href="./">ホーム</a>
			<a href="./signup">ユーザー新規登録</a><br />
		</div>

			<c:if test="${not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

		<c:forEach items="${userBranchDepartments}" var="userbranchdepartment">
		<div class="userlist">
			<label for="account">アカウント：</label><c:out value="${userbranchdepartment.account}" /><br />
			<label for="userName">ユーザ名：</label><c:out value="${userbranchdepartment.userName}" /><br />
			<label for="branchName">支社：</label><c:out value="${userbranchdepartment.branchName}" /><br />
			<label for="departmentName">部署：</label><c:out value="${userbranchdepartment.departmentName}" /><br />
			<c:if test="${userbranchdepartment.isStopped == 0}"><label for="isStopped">アカウント復活停止状態：</label>稼働中</c:if>
			<c:if test="${userbranchdepartment.isStopped == 1}"><label for="isStopped">アカウント復活停止状態：</label>停止中</c:if>
			<br />

				<div class="button">
					<form action="setting" method ="get">
						<input type="hidden" name="id" value="${userbranchdepartment.id}" />
						<input type="submit" value="編集"/>
					</form>
					<br />
					<c:if test="${loginUser.id != userbranchdepartment.id}">
						<c:if test="${userbranchdepartment.isStopped == 0}">
							<form action="stop" method ="post"onSubmit="return check()">
								<input type="hidden" name="id" value="${userbranchdepartment.id}" />
								<input type="hidden" name="isstopped" value="1" />
								<input type="submit" name="on" value="停止"/>
							</form>
						</c:if>
						<c:if test="${userbranchdepartment.isStopped == 1}">
							<form action="stop" method ="post"onSubmit="return check()">
								<input type="hidden" name="id" value="${userbranchdepartment.id}" />
								<input type="hidden" name="isstopped" value="0" />
								<input type="submit" name="off" value="復活"/>
							</form>
						</c:if>
					</c:if>
				</div>
			</div>
		</c:forEach>
		<div id="footer">
			<div class="copyright">Copyright(c)Nami Morioka</div>
		</div>
	</body>
</html>