<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/secondary.css" rel="stylesheet" type="text/css">
			<title>ユーザー新規登録</title>
	</head>
	<body>
		<div class="main-contents">

			<div id="header">
				<a href="./management">ユーザ管理</a>
			</div>

			<c:if test="${not empty errorMessages}">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>

			<form action="signup" method="post"><br />

				<label for="account">アカウント名</label>
				<input name="account" id="account" value="${user.account}" /> <br />

				<label for="password">パスワード</label>
				<input name="password" type="password" id="password" /> <br />

				<label for="passwordCheck">確認用パスワード</label>
				<input name="passwordCheck" type="password" id="passwordCheck" /> <br />

				<label for="name">名前</label>
				<input name="name" id="name" value="${user.name}" /><br />

				<label for="branchId">支社</label>
				<select name="branchId" id="branchId">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${user.branchId == branch.id}">
							<option value="${branch.id}" selected>${branch.name}</option>
						</c:if>
						<c:if test ="${user.branchId != branch.id}">
							<option value="${branch.id}">${branch.name}</option>
						</c:if>
					</c:forEach>
				</select><br />

				<label for="departmentId">部署</label>
				<select name="departmentId" id="departmentId">
					<c:forEach items="${departments}" var="department">
						<c:if test="${user.departmentId == department.id}">
							<option value="${department.id}" selected>${department.name}</option>
						</c:if>
						<c:if test="${user.departmentId != department.id}">
							<option value="${department.id}">${department.name}</option>
						</c:if>
					</c:forEach>
				</select><br />

				<input type="submit" value="登録" /> <br />

			</form>

			<div id="footer">Copyright(c)Nami Morioka</div>
		</div>
	</body>
</html>