<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<link href="./css/style.css" rel="stylesheet" type="text/css">
		<link href="popup_help.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="./js/jQuery.min.js"></script>
		<title>ホーム画面</title>
		<script type="text/javascript">
			function check(){
				if(window.confirm('削除してよろしいですか？')){
					return true;
				}
				else{
					window.alert('キャンセルされました');
					return false;
				}
			}
		</script>
	</head>

	<body>
		<div id="wrap">

			<div id="header">
				<a href="message">新規投稿</a>
				<a href="management">ユーザ管理</a>
				<a href="logout">ログアウト</a>
			</div>

				<c:if test="${not empty errorMessages}">
					<div class="errorMessages">
						<div class="errorMessages">
							<ul>
								<c:forEach items="${errorMessages}" var="errorMessage">
									<li><c:out value="${errorMessage}" />
								</c:forEach>
							</ul>
						</div>
						<c:remove var="errorMessages" scope="session" />
					</div>
				</c:if>
			<div id="sidenavi">
				<div class="selectMessage">
					<form action="index.jsp"method ="get">

						<label for="date">日時</label><br />
						<input name="startDate" type="date" id="startDate" value="${startDate}">～
						<input name="endDate" type="date" id="endDate" value="${endDate}"><br />
						<label for="category">カテゴリ</label>
						<input name="category" id="category" value="${category}">

						<input type="submit" name="slectMessage" value="絞込み"/><br />
					</form>
				</div>
			</div>
			<div id="userMessages">
				<c:forEach items="${messages}" var="message">
					<div class="message">
						<div class="account-name">
							<span class="alredyRead">[ 未読 ]</span> [<span class="popup_help"  data-message="クリックすると既読になります">?</span>]<br />
							<span class="name">ユーザ名：<c:out value="${message.userName}" /></span><br />
							<span class="title">件名：<c:out value="${message.title}" /></span><br />
							<span class="category">カテゴリ：<c:out value="${message.category}" /></span><br /><br />
							<label for="text">＜　投稿内容　＞</label>
						</div>
						<div class="text">
							<c:forEach items="${message.getTextMessage()}" var="text">
							<div><c:out value="${text}" /></div>
							</c:forEach>
						</div>
						<div class="date">投稿日時：<fmt:formatDate value="${message.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /><br /></div>
						<c:if test="${loginUser.id == message.userId}">
							<div class="deleteMessage">
								<form action="deleteMessage"method ="post"onSubmit="return check()">
									<input type="hidden" name="message.id" value="${message.id}" />
									<input type="submit" name="deleteMessage" value="削除"/><br />
								</form>
							</div>
						</c:if>
					</div>

					<c:forEach items="${comments}" var="comment">
						<c:if test="${message.id == comment.messageId}">
							<div class="comment">
								<span class="name">ユーザー名：<c:out value="${comment.name}" /></span><br />
								<span class="commentTitle">＜　コメント内容　＞</span>
								<div class="text">
									<c:forEach items ="${comment.getTextComment()}" var="text">
										<div><c:out value="${text}" /></div>
									</c:forEach>
								</div>
								<div class="date">コメント日時：<fmt:formatDate value="${comment.createdDate}" pattern="yyyy/MM/dd HH:mm:ss" /></div>
								<div class="deleteComment">
									<c:if test="${loginUser.id == comment.userId}">
										<form action="deleteComment"method ="post"onSubmit="return check()">
											<input type="hidden" name="comment.id" value="${comment.id}" />
											<input type="submit" name="deleteComment" value="削除"/><br />
										</form>
									</c:if>
								</div>
							</div>
						</c:if>
					</c:forEach>
					<div class="commentSubmit">
						<form action="comment" method="post">コメント入力欄<br />
							<textarea name="comment" cols="50" rows="5" class="comment">${comment.text}</textarea><br />
							<input type="hidden" name="message.id" value="${message.id}" />
							<input type="submit" value="コメント投稿" /> <br />
						</form>
					</div>
				</c:forEach>
				<div class="pagetop">PAGE TOP</div>
			</div>
			<div id="footer">
				<div class="copyright">Copyright(c)Nami Morioka</div>
			</div>
		</div>
	<script src="./js/script.js"></script>
	</body>
</html>