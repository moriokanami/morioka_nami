// クリックしたら発動
$(function(){
    $(".name").on("click", function() {
        alert("ユーザ名です！");
    });
});

// <span>要素をクリックしたら発動
$('.date').click(function() {
	// クリックした要素だけ文字色を赤字にする
	$(this).css('color','red');
});

// 既読確認
$('.alredyRead').click(function() {
	$(this).text('[ 既読 ]');
});

// 既読確認の方法 [?]
$(function() {

	  $(".popup_help").mouseover(function() {
	    var marginTop = 0;
	    var marginLeft = 20;
	    var speed = 300;

	    var popupObj = $(".popup_help_window");

	    if (!popupObj.length) {
	      // ウィンドウがなければ作成
	      popupObj = $("<p/>").addClass("popup_help_window").appendTo($("body"));
	    }

	    popupObj.text($(this).attr("data-message"));

	    var offsetTop = $(this).offset().top + marginTop;
	    var offsetLeft = $(this).offset().left + marginLeft;

	    popupObj.css({
	      "top": offsetTop,
	      "left": offsetLeft
	    }).show(speed);

	  }).mouseout(function() {
	    // 「?」からマウスが離れた場合、テキストを空にしてウィンドウを隠す
	    $(".popup_help_window").text("").hide("fast");
	  });

	});

$('#pagetop').click(function() {
	$("html,body").animate({
		scrollTop : $('#header').offset().top
	});
});