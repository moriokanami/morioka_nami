package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLXML;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.locks.AbstractQueuedLongSynchronizer;

import beans.UserMessage;
import exception.SQLRuntimeException;

public class UserMessageDao {

	public List<UserMessage> select(Connection connection, int num, String start, String end, String selectCategory) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("    messages.id AS id, ");
			sql.append("    messages.title AS title, ");
			sql.append("    messages.text AS text, ");
			sql.append("    messages.category AS category, ");
			sql.append("    messages.user_id AS user_id, ");
			sql.append("    messages.created_date AS created_date, ");
			sql.append("    users.name AS name ");
			sql.append("FROM messages ");
			sql.append("INNER JOIN users ");
			sql.append("ON messages.user_id = users.id ");
			sql.append("WHERE messages.created_date ");
			sql.append("BETWEEN  ?  ");
			sql.append("AND  ?  ");
			sql.append("AND messages.category ");
			sql.append("LIKE ? ");
			sql.append("ORDER BY created_date DESC limit " + num);

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, start);
			ps.setString(2, end);
			ps.setString(3, selectCategory);

			ResultSet rs = ps.executeQuery();

			List<UserMessage> messages = toUserMessages(rs);
			return messages;
		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserMessage> toUserMessages(ResultSet rs) throws SQLException {

		List<UserMessage> messages = new ArrayList<UserMessage>();
		try {
			while (rs.next()) {
				UserMessage message = new UserMessage();
				message.setId(rs.getInt("id"));
				message.setTitle(rs.getString("title"));
				message.setText(rs.getString("text"));
				message.setCategory(rs.getString("category"));
				message.setUserId(rs.getInt("user_id"));
				message.setCreatedDate(rs.getTimestamp("created_date"));
				message.setUserName(rs.getString("name"));

				messages.add(message);
			}
			return messages;
		} finally {
			close(rs);
		}
	}
}
