package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.User;
import exception.NoRowsUpdatedRuntimeException;
import exception.SQLRuntimeException;
import service.UserService;

public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("    account, ");
            sql.append("    password, ");
            sql.append("    name, ");
            sql.append("    branch_id, ");
            sql.append("    department_id, ");
            sql.append("    is_stopped, ");
            sql.append("    created_date, ");
            sql.append("    updated_date ");
            sql.append(") VALUES ( ");
            sql.append("    ?, ");                                  // account
            sql.append("    ?, ");                                  // password
            sql.append("    ?, ");                                  // name
            sql.append("    ?, ");                                  // branch_id
            sql.append("    ?, ");                                  // department_id
            sql.append("    DEFAULT,");                              // is_stopped デフォルト値=0 (稼働中)
            sql.append("    CURRENT_TIMESTAMP, ");                  // created_date
            sql.append("    CURRENT_TIMESTAMP ");                   // updated_date
            sql.append(");");

            ps = connection.prepareStatement(sql.toString());

            ps.setString(1, user.getAccount());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getName());
            ps.setInt(4, user.getBranchId());
            ps.setInt(5, user.getDepartmentId());

            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

    	List<User> users = new ArrayList<User>();

    	try {
    		while (rs.next()) {
    			User user = new User();
    			user.setId(rs.getInt("id"));
    			user.setAccount(rs.getString("account"));
    			user.setPassword(rs.getString("password"));
    			user.setName(rs.getString("name"));
    			user.setBranchId(rs.getInt("branch_id"));
    			user.setDepartmentId(rs.getInt("department_id"));
				user.setIsStopped(rs.getInt("is_stopped"));
				user.setCreatedDate(rs.getTimestamp("created_date"));
				user.setUpdatedDate(rs.getTimestamp("updated_date"));

				users.add(user);
    		}
    		return users;
    	} finally {
    		close(rs);
    	}
    }

    public User select(Connection connection, String account, String password)
			throws SQLRuntimeException {

		PreparedStatement ps = null;
		try {
			String sql = "SELECT * FROM users WHERE account = ?  AND password = ?";

			ps = connection.prepareStatement(sql);

			ps.setString(1, account);
			ps.setString(2, password);

			ResultSet rs = ps.executeQuery();

			List<User> users = toUserList(rs);
			if (users.isEmpty()) {
				return null;
    		} else if (2 <= users.size()) {
    			throw new IllegalStateException("ユーザーが重複しています");
			} else {
				return users.get(0);
			}
		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

    public User getUser(Connection connection, int id) {

    	PreparedStatement ps = null;
    	try {
    		String sql = "SELECT * FROM users WHERE id = ?";

    		ps = connection.prepareStatement(sql);
    		ps.setInt(1, id);

    		ResultSet rs =ps.executeQuery();
    		List<User> users = toUserList(rs);
    		if (users.isEmpty()) {
    			return null;
    		} else {
    			return users.get(0);
    		}
    	} catch (SQLException e) {
    		throw new SQLRuntimeException(e);
    	} finally {
    		close(ps);
    	}
    }

    public void update(Connection connection, User user) {

    	PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  account = ?");
			sql.append(", password = ?");
			sql.append(", name = ?");
			sql.append(", branch_id = ?");
			sql.append(", department_id = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");
			sql.append(" ; ");

			ps = connection.prepareStatement(sql.toString());

			ps.setString(1, user.getAccount());
			ps.setString(2, user.getPassword());
			ps.setString(3, user.getName());
			ps.setInt(4, user.getBranchId());
			ps.setInt(5, user.getDepartmentId());
			ps.setInt(6, user.getId());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }

    public void changeStatus(Connection connection, int userId, int changeStatus) {

    	PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("UPDATE users SET");
			sql.append("  is_stopped = ?");
			sql.append(", updated_date = CURRENT_TIMESTAMP");
			sql.append(" WHERE");
			sql.append(" id = ?");
			sql.append(" ; ");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1, changeStatus);
			ps.setInt(2, userId);

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }

    public User selectAccount(Connection connection, String account) {

    	PreparedStatement ps = null;
    	try {
    		StringBuilder sql = new StringBuilder();
    		sql.append("SELECT * FROM users WHERE account = ? ;");

    		ps = connection.prepareStatement(sql.toString());
    		ps.setString(1, account);
    		ResultSet rs = ps.executeQuery();
    		List<User> users = toUserList(rs);

    		if (users.isEmpty()) {
    			return null;
    		} else {
    			return users.get(0);
    		}
    	} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
    }
}