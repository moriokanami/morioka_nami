package dao;

import static utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import beans.UserBranchDepartment;

import exception.SQLRuntimeException;

public class UserBranchDao {

	public List<UserBranchDepartment> selects(Connection connection) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("SELECT ");
			sql.append("  users.id AS id, ");
			sql.append("  users.account AS account, ");
			sql.append("  users.name AS user_name, ");
			sql.append("  branches.name AS branch_name, ");
			sql.append("  departments.name AS department_name, ");
			sql.append("  users.is_stopped AS is_stopped, ");
			sql.append("  users.updated_date AS updated_date ");
			sql.append("FROM users ");
			sql.append("INNER JOIN branches ");
			sql.append("ON users.branch_id = branches.id ");
			sql.append("INNER JOIN departments ");
			sql.append("ON users.department_id = departments.id ;");

			ps = connection.prepareStatement(sql.toString());
			ResultSet rs = ps.executeQuery();

			List<UserBranchDepartment> userBranchDepartments = toUserBranchDepartments(rs);
			return userBranchDepartments;

		} catch(SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}

	private List<UserBranchDepartment> toUserBranchDepartments(ResultSet rs) throws SQLException {

		List<UserBranchDepartment> userBranchDepartments = new ArrayList<UserBranchDepartment>();
		try {
			while (rs.next()) {
				UserBranchDepartment userBranchDepartment = new UserBranchDepartment();
				userBranchDepartment.setId(rs.getInt("id"));
				userBranchDepartment.setAccount(rs.getString("account"));
				userBranchDepartment.setUserName(rs.getString("user_name"));
				userBranchDepartment.setBranchName(rs.getString("branch_name"));
				userBranchDepartment.setDepartmentName(rs.getString("department_name"));
				userBranchDepartment.setIsStopped(rs.getByte("is_stopped"));
				userBranchDepartment.setUpdatedDate(rs.getTimestamp("updated_date"));

				userBranchDepartments.add(userBranchDepartment);
			}
			return userBranchDepartments;
		} finally {
			close(rs);
		}
	}
}