package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/stop" })
public class StopServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

    	int userId;
    	userId = Integer.parseInt(request.getParameter("id"));

    	String getStatus = request.getParameter("isstopped");

    	if(getStatus.equals("0")) {
    		int changeStatus = 0;
    		new UserService().changeStatus(userId, changeStatus);
    		response.sendRedirect("/morioka_nami/management");

    	} else if(getStatus.equals("1")) {
    		int changeStatus = 1;
    		new UserService().changeStatus(userId,changeStatus);
    		response.sendRedirect("/morioka_nami/management");
    	}
    }
}
