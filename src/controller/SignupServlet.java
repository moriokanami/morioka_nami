package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.catalina.connector.Request;
import org.apache.commons.lang.StringUtils;

import beans.User;
import beans.Branch;
import beans.Department;
import service.UserService;
import service.BranchService;
import service.DepartmentService;


@WebServlet(urlPatterns = { "/signup" })
public class SignupServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
	        throws IOException, ServletException {

		List<Branch> branches = new ArrayList<>();
		List<Department> departments = new ArrayList<>();
		branches = new BranchService().getBranches();
		departments = new DepartmentService().getDepartments();

	    request.setAttribute("branches", branches);
	    request.setAttribute("departments", departments);
	    request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<Branch> branches = new ArrayList<>();
		List<Department> departments = new ArrayList<>();
		List<String> errorMessages = new ArrayList<String>();

		branches = new BranchService().getBranches();
		departments = new DepartmentService().getDepartments();

		User user = getUser(request);

		if ( !isValid(user, errorMessages, request)) {
			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("user", user);
		    request.setAttribute("branches", branches);
		    request.setAttribute("departments", departments);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
			return;
		} else {
			new UserService().insert(user);
			response.sendRedirect("management");
		}
	}

	private User getUser(HttpServletRequest request) throws IOException, ServletException {

		User user = new User();
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setName(request.getParameter("name"));
		user.setBranchId(Integer.parseInt(request.getParameter("branchId")));
		user.setDepartmentId(Integer.parseInt(request.getParameter("departmentId")));

		return user;
	}

	private boolean isValid(User user, List<String> errorMessages, HttpServletRequest request) {

		String account = user.getAccount();
		String password = user.getPassword();
		String name = user.getName();
		String passwordCheck = request.getParameter("passwordCheck");
		int branchId = user.getBranchId();
		int departmentId = user.getDepartmentId();
		User checkAccountUser = new UserService().checkAccount(account);

		if (checkAccountUser != null) {
			errorMessages.add("アカウントが重複しています");
		}

		if (branchId == 1 && departmentId >= 3) {
			errorMessages.add("支社と部署の組み合わせが不正です");
		}
		if (branchId >= 2 && departmentId <= 2) {
			errorMessages.add("支社と部署の組み合わせが不正です");
		}

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウントを入力してください");
		} else if (account.length() < 6) {
		    errorMessages.add("アカウントは6文字以上で入力してください");
		} else if (20 < account.length()) {
		    errorMessages.add("アカウントは20文字以下で入力してください");
		} else if ( !account.matches("^[a-zA-Z0-9]+$")) {
		    errorMessages.add("アカウントは半角英数字で入力してください");
		}

		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードを入力してください");
		} else if (password.length() < 6) {
		    errorMessages.add("パスワードは6文字以上で入力してください");
		} else if (20 < password.length()) {
		    errorMessages.add("パスワードは20文字以下で入力してください");
		} else if ( !password.matches("^[a-zA-Z0-9!-/:-@¥[-`{-~]]+$")) {
			errorMessages.add("パスワードは半角英数字で入力してください");
		}
		if ( !StringUtils.isEmpty(password) && !password.equals(passwordCheck)) {
			errorMessages.add("パスワードが確認用パスワードと一致していません");
		}
		if (StringUtils.isEmpty(name)) {
			errorMessages.add("ユーザ名を入力してください");
		} else if (10 < name.length()) {
			errorMessages.add("ユーザ名は10文字以下で入力してください");
		}
		if (errorMessages.size() == 0) {
			return true;
		}
		return false;
	}
}