package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.User;
import beans.UserBranchDepartment;
import service.UserService;

@WebServlet(urlPatterns = { "/management" })
public class ManagementServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

    	HttpSession session = request.getSession();
    	User loginUser = (User)session.getAttribute("loginUser");

        List<UserBranchDepartment> userBranchDepartments= new UserService().getUserBranchDepartments();

        request.setAttribute("userBranchDepartments", userBranchDepartments);
        session.setAttribute("loginUser", loginUser);
        request.getRequestDispatcher("management.jsp").forward(request, response);
    }
}