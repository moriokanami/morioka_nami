package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.User;
import service.UserService;

@WebServlet(urlPatterns = { "/login" })
public class LoginServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		String account = request.getParameter("account");
		String password = request.getParameter("password");
		List<String> errorMessages = new ArrayList<String>();

		User user = new UserService().select(account, password);

		if (!isValid(user, errorMessages, account, password)) {
			request.setAttribute("account", account);
			session.setAttribute("errorMessages", errorMessages);
			request.getRequestDispatcher("login.jsp").forward(request, response);
			return;
		}

		session.setAttribute("loginUser", user);
		response.sendRedirect("./");

	}

	private boolean isValid(User user, List<String> errorMessages, String account, String password) {

		if (StringUtils.isEmpty(account)) {
			errorMessages.add("アカウントが入力されていません");
		}
		if (StringUtils.isEmpty(password)) {
			errorMessages.add("パスワードが入力されていません");
		}
        if(StringUtils.isEmpty(account) || StringUtils.isEmpty(password)) {
        	return false;
        }

		if (user == null) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}  else if (user.getIsStopped() == 1) {
			errorMessages.add("アカウントまたはパスワードが誤っています");
		}

		if (errorMessages.size() == 0) {
			return true;
		}
		return false;
	}
}