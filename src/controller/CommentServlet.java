package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.css.ElementCSSInlineStyle;

import beans.Comment;
import beans.User;
import service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("home.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		User user = (User)session.getAttribute("loginUser");

		Comment comment = new Comment();
		comment.setText(request.getParameter("comment"));
		comment.setUserId(user.getId());
		comment.setMessageId(Integer.parseInt(request.getParameter("message.id")));

		if (isValid(request, errorMessages)) {
			new CommentService().insert(comment);
			response.sendRedirect("./");

		} else {
			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("comment", comment);
			request.getRequestDispatcher("home.jsp").forward(request, response);
			return;
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> errorMessages) {

		String comment = request.getParameter("comment");

		if (StringUtils.isEmpty(comment)) {
		    errorMessages.add("コメントを入力してください");
		} else if (1000 < comment.length()) {
			errorMessages.add("コメントは500文字以下で入力してください");
		} else if (comment.matches("[\\s|　]+")) {
			errorMessages.add("コメントを入力してください");
		}

		if (errorMessages.size() == 0) {
		    return true;
		}

		return false;

	}
}