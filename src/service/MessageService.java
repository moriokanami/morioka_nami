package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.Date;
import java.util.List;

import com.mysql.jdbc.StringUtils;

import java.text.SimpleDateFormat;

import beans.Message;
import beans.UserMessage;
import dao.MessageDao;
import dao.UserMessageDao;

public class MessageService {

	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
		} catch(Error e) {
			rollback(connection);
		} finally {
			close(connection);
		}
	}

	public List<UserMessage> select(String startDate, String endDate, String category) {
		final int LIMIT_NUM = 1000;
		String start=null;
		String end = null;
		String selectCategory = null;

		Connection connection = null;
		try {
			if (StringUtils.isNullOrEmpty(startDate)) {
				start = ("2020-01-01 00:00:00");
			} else {
				start = startDate + (" 00:00:00");
			}

			if (StringUtils.isNullOrEmpty(endDate)) {
				Date date = new Date();
				end = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date);
			} else {
				end = endDate + (" 23:59:59");
			}
			if (StringUtils.isNullOrEmpty(category)) {
				selectCategory = ("%%");
			} else {
				selectCategory =("%" +category + "%");
			}

			connection = getConnection();
			List<UserMessage> messages = new UserMessageDao().select(connection, LIMIT_NUM, start, end, selectCategory);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}

	}

	public void delete(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().delete(connection, message);
			commit(connection);
		} catch(RuntimeException e) {
			rollback(connection);
		} catch(Error e) {
			rollback(connection);
		} finally {
			close(connection);
		}

	}
}
