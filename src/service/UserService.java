package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import beans.User;
import beans.UserBranchDepartment;
import dao.UserDao;
import dao.UserBranchDao;
import exception.SQLRuntimeException;
import utils.CipherUtil;

public class UserService {

	public void insert(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().insert(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	public User select(String account, String password) throws SQLRuntimeException{

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(password);

			connection = getConnection();
			User user = new UserDao().select(connection, account, encPassword);
			commit(connection);

			return user;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	// ユーザー一覧取得
	public List<UserBranchDepartment> getUserBranchDepartments() {

		Connection connection = null;
		try {
			connection = getConnection();
			List<UserBranchDepartment> userBranchDepartments = new UserBranchDao().selects(connection);
			commit(connection);
			return userBranchDepartments;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;

		} catch (Error e) {
			rollback(connection);
			throw e;

		} finally {
			close(connection);
		}
	}

	// 変更したいユーザーの情報取得
    public User getUser(int id) {

		Connection connection = null;
		try {
			connection = getConnection();
			User user = new UserDao().getUser(connection, id);

			commit(connection);
			return user;

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    // ユーザー編集
    public void update(User user) {

		Connection connection = null;
		try {
			// パスワード暗号化
			String encPassword = CipherUtil.encrypt(user.getPassword());
			user.setPassword(encPassword);

			connection = getConnection();
			new UserDao().update(connection, user);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    // ユーザー復活停止
    public void changeStatus(int userId, int changeStatus) {

		Connection connection = null;
		try {
			connection = getConnection();
			new UserDao().changeStatus(connection, userId,changeStatus);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

    //ユーザー重複チェック
    public User checkAccount(String account) {

    	Connection connection = null;
    	try {
    		connection = getConnection();
    		User user = new UserDao().selectAccount(connection, account);
    		commit(connection);
    		return user;
    	} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
    }
}