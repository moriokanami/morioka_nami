package beans;

import java.io.Serializable;
import java.util.Date;

public class UserBranchDepartment implements Serializable {

	private int id;
	private String account;
	private String userName;
	private String branchName;
	private String departmentName;
	private byte isStopped;
	private Date updatedDate;

	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getBranchName() {
		return branchName;
	}
	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}
	public String getDepartmentName() {
		return departmentName;
	}
	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}
	public byte getIsStopped() {
		return isStopped;
	}
	public void setIsStopped(byte isStopped) {
		this.isStopped = isStopped;
	}
}
